﻿#include <iostream>
#include <string>

const int N = 19;

//Функция вывода чётных или нечётных чисел
void sequence(int number, int parOdd)//parOdd - parity or oddness(2 or 1)
{
    if (number > 0) {
        std::cout << "Entered number " << number << ", parity or oddness = " << parOdd << std::endl;
        std::cout << "------------------------------------" << std::endl;
     
        if (parOdd == 1|| parOdd == 2) {
                for (int i = 1; i <= number; i++) {
                             if (i % 2 == 1 && parOdd == 1) {
                                 std::cout << i << std::endl;
                                  }
                             if (i % 2 == 0 && parOdd == 2) {
                                 std::cout << i << std::endl;
                                  }
                                                    }

                    std::cout << "=============" << std::endl << std::endl;
        }
        else {
            std::cout << "Incorrect entered  parity or oddness, must be parity - 2, oddness - 1" << std::endl;
        }
        
    }
    else {
        std::cout << "Incorrect entered, number must be >0" << std::endl;
    }
}

int main()
{
    sequence(N, 1);

    sequence(15, 1);
    sequence(23, 2);
    sequence(-23, 2);
    sequence(23, 3);

    return 0;
}
